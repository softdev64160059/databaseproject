/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sasipa.databaseproject1.dao;

import com.sasipa.databaseproject1.model.User;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    T save(T obj);
    T  update(T obj);
    List<T> getAllOrderBy(String where, String order);
} 
