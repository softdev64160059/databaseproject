/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sasipa.databaseproject1.service;

import com.sasipa.databaseproject1.dao.UserDao;
import com.sasipa.databaseproject1.model.User;

/**
 *
 * @author Lenovo
 */
public class UserService {
    public  User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if(user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
